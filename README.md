How to setup:
  Have a unix system setup.  Have npm installed on the unix system(apt-get install nodejs  and npm if needed).

  Download sourcetree, login with bitbucket account/correct email(important as sourcetree can hold multiple emails and accounts that sometimes conflict with eachother), if you have access to the project you will be able to go to the remote tab in source tree.  Then clone the repository(choose the branch you need, develop for develop, master/release for releasing/testing).  Make note of where it is in your file system.  Open the folder(budgetbuddy) in VS Code.  Open a terminal in VS Code, and run "npm install", this will get and download all the packages we need for the project.  
  
  -To watch all files and recompile/compile them when you save/change any of them run "npm run tsc" in the terminal.  
  -To run the client run "npm run http", this will not update when changes are made.  
  -To run the project client so that it does update when changes are made it updates, run "npm run start" in the terminal, this will run both npm run tsc and npm run project concurrently(at the same time).  Go to the link generate by the terminal.
  -To run the server run "node dist/src/server/server.js"(only use this after compiling).
  -To run the whole project, in this order run "npm run tsc" then once that is done "npm run project" and "npm run server" each of these need their own terminal. 

Notes:
  You can find the developer commands(the commands used above with "npm run" before them) under package.json scripts.

  Notice the file system.  There is a place for everything.
  Generally speaking keep files in one language/seperated by task/intended use.
  Use proper naming convention/coding conventions.
  (https://github.com/unional/typescript-guidelines/blob/master/pages/default/draft/naming-conventions.md)

  Fetch: will show the changes others have pushed to the repository, in the commits log.
  Pull: will take all the changes that have happened since the last time you cloned/pulled, and put them on your system/working copy.
  Push: will put all of your changes that have been commited, into the branch of the repository that you are on.  For everyone to access.

  To create a feature branch, in sourceTree under budgetbuddy, click git-flow button, then if not already setup hit ok(dont change any settings), then hit git-flow again, and click start new feature.  It will prompt you for the name of the new branch, name it the feature you are trying to implement(with correct naming conventions), for example: "startButton".  


Commands to try:

- cntrl + alt + {up arrow/down arrow)
- cntrl + shift + {all arrow directions}
- cntrl + u
- highlight something, tab and shift tab

Things to try:
- in the terminal, hit the book looking icon that is right of the plus and left of the garbage can.  (in vs codes terminal)

Things to know about:
- Checkout how the git ignore works