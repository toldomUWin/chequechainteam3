const mysql = require('mysql');
import utils from '../Util/utilities';

export default class dataBase {
  //chequeChainTeam3
  private host = "chequechainteam3db.c0h433j2sjeq.us-east-2.rds.amazonaws.com";
  private user = "CCT3";
  private password = "CCT3Database";
  public database = "ChequeChainTeam3DB";
  private connected = false;
  private userDataBase;

  constructor() { }

  public setupConnection() {
    this.userDataBase = mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.password,
      database: this.database
    });
    this.userDataBase.connect(function (err) {
      if (err) throw err;
      console.log("Database connection established!");
    });
  }

  private makeSqlQuery(sql, fnc?): any {
    this.userDataBase.query(sql, function (err, result) {
      if (fnc) {
        fnc(err, result);
      }
      return result;
      if (err) throw err;
    });
  }

  /***
   * This will insert one user to the user table.  Dont worry about the ID it will auto increment.
   */
  public async insertUser(username: string, hash: string, firstName: string, lastName: string, bankName: string) {

    let values = `VALUES("${firstName}","${lastName}","${bankName}","${hash}",NOW(),"${username}")`;
    let sql = this.userTableInsertString + ' ' + values;
    
    this.makeSqlQuery(sql);
  }

  public verifyUser(username, password, fnc?) {

    let query = `SELECT * FROM users where username = '${username}'`;
    this.userDataBase.query(query, function (error, results) {
      if (error) throw error;
      
      //So if the account does not exist, we manually set the restlt to be DNE to avoud a crash
      if (results[0] === undefined) {
        fnc("NoSuchUser");
      } else if (results[0].passwordHash == password) {
        fnc("PasswordCorrect");
      } else {
        fnc("PasswordIncorrect");
      }
    });
  }

  /*
    This will query the database for the inputed email, and return the email if it exists in the database
  */
  private get userTableInsertString() {
    return 'INSERT INTO users(firstname,lastname,bankname,passwordHash,createDate,username)';
  }

}