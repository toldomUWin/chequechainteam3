import dataBase from "./dataBase";
import axios from "axios"
import utils from "../Util/utilities"
// server.js
const express = require('express'),
  cors = require("cors"),
  server = express();
//creating the datasctructure that holds the database
//connecting to the database
var userDataBase = new dataBase();
userDataBase.setupConnection();
server.use(express.json());
server.use(cors());
server.set('port', process.env.PORT || 3000);
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // SSL bypass
process.on('unhandledRejection', (error, promise) => {
  console.log(' Oh Lord! We forgot to handle a promise rejection here: ', promise);
  console.log(' The error was: ', error );
});
server.post('/signup', async (request, response) => {
  let firstName = request.body.signupInfo.firstName;
  let lastName = request.body.signupInfo.lastName;
  let userName = request.body.signupInfo.userName;
  let bankName = request.body.signupInfo.bankName;
  let email = request.body.signupInfo.email;
  let password = request.body.signupInfo.password;

  userDataBase.insertUser(userName, password, firstName, lastName, bankName).then(() => {
    response.send(200);
  });

});

server.get('/mine', async (request, response) => {

  sendMinerequest().then(() => {
    response.send(200);
  });;

});

server.post('/login', (request, response) => {
  let username = request.body.loginInfo.username;
  let password = request.body.loginInfo.password;

  userDataBase.verifyUser(username, password, (dbResponse) => {
    if (dbResponse === "PasswordCorrect") {
      console.log("User Verified")
      response.sendStatus(200)
    } else if (dbResponse === "PasswordIncorrect") {
      console.log("IncorrectPassword")
      response.sendStatus(201)
    } else if (dbResponse === "NoSuchUser") {
      console.log("That user does not exist")
      response.sendStatus(203)
    } else {
      console.log("Unknown login error")
      response.sendStatus(204)
    }
  });

  // response.send(200);
});

server.post('/addCheque', async (request, response) => {

  let Info = {
    sender: request.body.chequeInfo.sender,
    recipient: request.body.chequeInfo.reciever,
    chequeId: request.body.chequeInfo.chequeId,
    amount: request.body.chequeInfo.balance,
    transactionNumber: request.body.chequeInfo.transactionNumber,
    bankId: request.body.chequeInfo.bankId,
    date: request.body.chequeInfo.date,
    payorSign: request.body.chequeInfo.payorSign
  };
  console.log(Info);
  let finInstNum = request.body.chequeInfo.balance;
  let tranNum = request.body.chequeInfo.transactionNumber;
  let accountId = request.body.chequeInfo.accountId;


  let chequeInfo = {
    recipient: request.body.chequeInfo.reciever,
    amount: request.body.chequeInfo.balance
  };
  let amount = request.body.chequeInfo.balance;
  let recipient = request.body.chequeInfo.reciever;

  checkInfoAndAddTransaction(finInstNum, tranNum, accountId, Info).then(() => {
    response.send('OK');
  });

  // addTransaction(recipient, amount).then(() => {
  //   response.send('OK');
  // });;
  // console.log(chequeInfo);

});

server.get('/getTransactionPool', async (request, response) => {
  sendToPool((apiResponse) => { response.send(apiResponse.data) });
});

server.get('/getBlocks', async (request, response) => {
  getBlocks((apiResponse) => { response.send(apiResponse.data) });
});
// server.post('/getBlocks', async (request, response) => {
//   getAccounts((apiResponse) => { response.send(apiResponse.data) });
// });
server.post('/getBanks', async (request, response) => {
  getBanks((apiResponse) => { response.send(apiResponse.data) });
});

// Express error handling middleware
server.use((request, response) => {
  response.type('text/plain');
  response.status(505);
  response.send('Error page');
});

let addTransaction = async (info) => {
  let res = await axios.post(utils.blockChainAddress + "/api/transact",
    info).then((response) => {
      if (response.statusText === 'OK') {
        console.log("WOOT");
      } else {
        console.log("ERROR: Incorrect Credentials");
        console.log(response);
      }
    }, (error) => {
      console.log(error);
    });
}
let checkInfoAndAddTransaction = async (finInstNum, tranNum, accountId, info) => {
  let res = await axios.post(utils.apiAddress + "/cheques", {
    finInstNum, tranNum, accountId
  }).then((response) => {
    if (response.status === 201) {
      console.log("MADE IT");
      addTransaction(info)
    } else {
      console.log("ERROR: Incorrect Credentials");
      console.log(response.statusText);
    }
  }, (error) => {
    console.log(error);
  });
}

let sendMinerequest = async () => {
  let res = await axios.get(utils.blockChainAddress + "/api/mine-transactions").then((response) => {
    if (response.statusText === 'OK') {
      // console.log(response );
    } else {
      console.log("ERROR: Incorrect Credentials");
      console.log(response);
    }
  }, (error) => {
    console.log(error);
  });
}

let sendToPool = async (fnc) => {
  let res = await axios.get(utils.blockChainAddress + "/api/transaction-pool-map").then((response) => {
    if (response.statusText === 'OK') {
      fnc(response);
      console.log(response.data);
    } else {
      console.log("ERROR: Incorrect Credentials");
      console.log(response);
    }
  }, (error) => {
    console.log(error);
  });
}

let getBlocks = async (fnc) => {
  let res = await axios.get(utils.blockChainAddress + "/api/blocks").then((response) => {
    if (response.statusText === 'OK') {
      fnc(response);
      console.log(response.data);
    } else {
      console.log("ERROR: Incorrect Credentials");
      console.log(response);
    }
  }, (error) => {
    console.log(error);
  });
}
let getAccounts = async (finInstNum, tranNum, bodyLimit, pageLimit,fnc) => {
  //?finInstNum=1&tranNum=1&bodyLimit=10000&pageLimit=10000
  // https://chequechain.wasplabs.ca/accounts?finInstNum=11&tranNum=1&bodyLimit=10000&pageLimit=10000
  let res = await axios.get(utils.apiAddress + "/accounts?" + `finInstNum="${finInstNum}"&tranNum="${tranNum}"&bodyLimit="${bodyLimit}","${pageLimit}"`).then((response) => {
    if (response.statusText === 'OK') {
      fnc(response);
      console.log(response.data);
    } else {
      console.log("ERROR: Incorrect Credentials");
      console.log(response);
    }
  }, (error) => {
    console.log(error);
  });
}

let getBanks = async (fnc) => {
  let res = await axios.get(utils.apiAddress + "/banks?bodyLimit=10&pageLimit=1").then((response) => {
    if (response.statusText === 'OK') {
      fnc(response);
      console.log(response.data);
    } else {
      console.log("ERROR: Incorrect Credentials");
      console.log(response);
    }
  }, (error) => {
    console.log(error);
  });
}

// Binding to a port
server.listen(3000, () => {
  console.log('Express server started at port 3000');
});