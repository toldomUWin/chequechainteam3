import * as crypto from 'crypto';

export default class Util {
  private static live = true;
  public static apiAddress = "https://chequechain.wasplabs.ca";
  public static ipAddress = Util.live ? "http://3.15.170.157:3000" : "http://localhost:3000";
  public static blockChainAddress = Util.live ? "http://3.15.170.157:3001" : "http://localhost:3001";
  constructor() {

  };
  public static hash(...inputs) {
    const hash = crypto.createHash('sha256');

    hash.update('alphaOmega' + inputs.sort().join(' '));

    return hash.digest('hex');
  };

};  