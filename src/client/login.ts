import httpsFunctions from "./httpsFunctions"

export default class loginHandler {
  //TODO rename modal and span
  private signInButton;
  private signUpButton;
  private enterButton;
  private span;
  private signInInput;
  private signUpInput;
  private httpsFunctions: httpsFunctions;
  private signingIn: boolean;

  constructor() {
    this.httpsFunctions = new httpsFunctions();
    this.getElements();
    this.setupButtonClicks();
  }

  private getElements() {

    this.signInButton = document.getElementById("signIn");
    this.signUpButton = document.getElementById("signUp");
    this.enterButton = document.getElementById("enter");
    // Get the <span> element that closes the modal
    this.span = (<HTMLInputElement>document.getElementsByClassName("close")[0]);
    this.signInInput = document.getElementById("signinint");
    this.signUpInput = document.getElementById("signupint");
  }

  private setupButtonClicks() {
   
    this.updateInputElements(0, false, false, true);

    // Get input fields when user clicks on sign up button
    this.signInButton.onclick = () => {
      this.updateInputElements(0, true, false, true);
      this.signingIn = true;
    }

    // Get input fields when user clicks on sign up button
    this.signUpButton.onclick = () => {
      this.updateInputElements(0, false, true, true);
      this.signingIn = false;
    }
    // User input is input
    this.enterButton.onclick = () => {
      if (this.signingIn) {
        this.httpsFunctions.login(this.signinData[0], this.signinData[1])
      } else {
        this.httpsFunctions.signup(this.signupData[0], this.signupData[1],this.signupData[2], this.signupData[3],this.signupData[4],this.signupData[5])
      }
    }


  }

  private updateInputElements(modalBool, signInBool, signUpBool, enterbtnBool) {
  
    if (signInBool !== 0) {
      this.signInInput.style.display = signInBool ? "block" : "none";
    }
    if (signUpBool !== 0) {
      this.signUpInput.style.display = signUpBool ? "block" : "none";
    }
    if (enterbtnBool !== 0) {
      this.enterButton.style.display = enterbtnBool ? "block" : "none";
    }
  }
  private get signinData() {
    let inputs = <HTMLInputElement>this.signInInput;
    let inputsValues = [inputs[0].value, inputs[1].value]
    return inputsValues;
  }
  private get signupData() {
    let inputs = <HTMLInputElement>this.signUpInput;
    let inputsValues = [inputs[0].value, inputs[1].value, inputs[2].value, inputs[3].value, inputs[4].value, inputs[5].value]
    return inputsValues;
  }
}
//making an instance of the class. ADDED This calls the instructor
var handler = new loginHandler();