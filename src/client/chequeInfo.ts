import httpsFunctions from "./httpsFunctions"
import getCheckInfo from "./getcheckInfo"

export default class checkInfo {
    private enterButton;
    private inputForm;
    private postHandler;
    private mineButton;
    constructor() {
        console.log("here");
        this.getElements();
        console.log(this.chequeData);
        let log = new httpsFunctions();
        this.postHandler = new getCheckInfo();
        this.setupEnterFunction();
        this.setupMineFunction();
    }

    private getElements() {

        this.inputForm = (<HTMLInputElement>document.getElementById("inputArea"));
        this.enterButton = (<HTMLInputElement>document.getElementById("enter"));
        this.mineButton = (<HTMLInputElement>document.getElementById("mine"));

        console.log("here");
        console.log(this.inputForm);
        //not sure what this is for.  I dont think we need any of it for check verifaction.
        // let sender = (<HTMLInputElement>document.getElementById("sender"));
        // let recipient = (<HTMLInputElement>document.getElementById("recipient"));
        // let balance = (<HTMLInputElement>document.getElementById("balance"));
        // let date = (<HTMLInputElement>document.getElementById("date"));
        // let payerSign = (<HTMLInputElement>document.getElementById("payorSign"));
        // let chequeId = (<HTMLInputElement>document.getElementById("chequeId"));
        // let transactionNumber = (<HTMLInputElement>document.getElementById("transactionNum"));
        // let accountId = (<HTMLInputElement>document.getElementById("accountId"));
        let bankID = (<HTMLInputElement>document.getElementById("bankId"));
    }

    private setupEnterFunction() {
        this.enterButton.onclick = () => {
            this.postHandler.sendChequeInfo(this.chequeData);
        }
    }
    private setupMineFunction() {
        this.mineButton.onclick = () => {
            this.postHandler.startMineBlock();
            console.log("here");
        }
    }

    private get chequeData() {
        let inputs = this.inputForm;
        let inputsValues = [];
        inputsValues.push(inputs[0].value);
        inputsValues.push(inputs[1].value);
        inputsValues.push(inputs[2].value);
        inputsValues.push(parseInt(inputs[3].value));
        inputsValues.push(inputs[4].value);
        inputsValues.push(inputs[5].value);
        inputsValues.push(inputs[6].value);
        inputsValues.push(inputs[7].valueAsNumber);
        inputsValues.push(inputs[8].value);
        this. clearInfo();

        return inputsValues;
    }
    private clearInfo(){
        this.inputForm.reset();
    }

}

let chequeData = new checkInfo();