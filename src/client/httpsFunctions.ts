import utils from '../Util/utilities'
import axios from "axios"
import { resolve } from 'dns';

export default class httpsFunctions {
    private wait: boolean = false;
    /**
     * This class will take input and make requests to the server.  On succesful 
     * call it will save the sessionKey
     */
    constructor() { }

    public async login(username, password) {

        if (username === "") {
            alert("Plesase enter a username")
            return
        } else if (username.includes(" ")) {
            alert("Username cannot contain spaces")
            return
        } else if (password === "") {
            alert("Please enter a password")
            return
        }

        if (this.wait) {
            return;
        } else {
            this.wait = true;
        }
        let loginInfo = {
            username: username,
            password: utils.hash(password)
        };
        let res = await axios.post(utils.ipAddress + '/login', {
            loginInfo
        }).then((response) => {

            if (response.statusText === 'OK') {
                this.resetStorage();
                this.storeUserKey(response.data);
                console.log("User verified, logging in")
                window.location.assign("inputCheque.html");
            } else if (response.statusText == "Created") {
                console.log("ERROR: Incorrect Password")
                alert("Incorrect Password")
            } else {
                console.log("ERROR: No such user");
                alert("No account exists under that username");
            }

            this.wait = false;
        }, (error) => {
            console.log(error);
            this.wait = false;
        });
    }

    public async signup(firstName, lastName, bankName, email, username, password) {

        if (username === "") {
            alert("Please enter a username")
            return
        } else if (username.includes(" ")) {
            alert("Username cannot contain spaces")
            return
        } else if (password === "") {
            alert("Please enter a password")
            return
        }


        if (this.wait) {
            return;
        } else {
            this.wait = true;
        }
        let signupInfo = {
            firstName: firstName,
            lastName: lastName,
            userName: username,
            email: email,
            bankName: bankName,
            password: utils.hash(password)
        };

        let res = await axios.post(utils.ipAddress + '/signup', {
            signupInfo
        }).then((response) => {
            if (response.statusText === 'OK') {
                this.resetStorage();
                this.storeUserKey(response.data);
                window.location.assign("inputCheque.html");
            } else {
                alert("Something Went Wrong Please Try Again");
            }
            this.wait = false;
        }, (error) => {
            console.log(error);
            this.wait = false;
        });
    }

    public async getTransData(callback: (dataObject) => any) {
        if (this.wait) {
            return;
        } else {
            this.wait = true;
        }
        
        let data;
        let res = await axios.get(utils.ipAddress + '/getTransactionPool').then((response) => {
            if (response.statusText === 'OK') {
                //(<HTMLOutputElement>document.getElementById("transPoolData")).innerText = JSON.stringify(response.data);
                console.log(response.data, "http");
                callback(response.data);
            } else {
                console.error("i dont work");
            }

            this.wait = false;
        }, (error) => {
            console.log(error);
            this.wait = false;
        });
    }

    public async getMinedBlocks(callback: (dataObject) => any) {
        if (this.wait) {
            return;
        } else {
            this.wait = true;
        }
        
        let data;
        let res = await axios.get(utils.ipAddress + '/getBlocks').then((response) => {
            if (response.statusText === 'OK') {
                //(<HTMLOutputElement>document.getElementById("transPoolData")).innerText = JSON.stringify(response.data);
                console.log(response.data, "http");
                callback(response.data);
            } else {
                console.error("i dont work");
            }

            this.wait = false;
        }, (error) => {
            console.log(error);
            this.wait = false;
        });
    }
    public async getBanks(callback: (dataObject) => any) {
        if (this.wait) {
            return;
        } else {
            this.wait = true;
        }
        
        let data;
        let res = await axios.get(utils.ipAddress + '/getBanks').then((response) => {
            if (response.statusText === 'OK') {
                //(<HTMLOutputElement>document.getElementById("transPoolData")).innerText = JSON.stringify(response.data);
                console.log(response.data, "http");
                callback(response.data);
            } else {
                console.error("i dont work");
            }

            this.wait = false;
        }, (error) => {
            console.log(error);
            this.wait = false;
        });
    }

    private storeUserKey(hash) {
        localStorage.setItem('sessionKey', hash);
    }

    public get UserKey() {
        return localStorage.getItem('sessionKey');
    }

    public resetStorage() {
        localStorage.clear();
    }
}