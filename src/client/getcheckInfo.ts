import utils from '../Util/utilities'
import axios from "axios"

export default class getcheckInfo {
    private hold: boolean = false;
    /**
     * This class will take input and make requests to the server.  On succesful 
     * call it will save the sessionKey
     */
    constructor() {
        //
    }

    public async sendChequeInfo(info) {
        if (this.hold) {
            return;
        } else {
            this.hold = true;
        }
        let chequeInfo = {
            sender: info[1],
            reciever: info[0],
            chequeId: info[2],
            balance: info[3],
            transactionNumber: info[4],
            accountId: info[5],
            bankId: info[6],
            date: info[7],
            payorSign: info[8],
        };
        ///////// HERE////////
        let res = await axios.post(utils.ipAddress + "/addCheque", {
            chequeInfo
        }).then((response) => {
            if (response.statusText === 'OK') {
                alert("Check Succesfully Posted on Transaction Pool")
            } else {
                // alert("INVALID CHECK")

                console.log("ERROR: Incorrect Credentials");
                console.log(response);
            }

            this.hold = false;
        }, (error) => {
            console.log(error);
            this.hold = false;
        });


    }
    public async startMineBlock() {
        let res = await axios.get(utils.ipAddress + "/mine", {
        }).then((response) => {
            if (response.statusText === 'OK') {

            } else {
                console.log("ERROR: Incorrect Credentials");
                console.log(response);
            }
        }, (error) => {
            console.log(error);
        });


    }
}