var path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    login: './dist/src/client/login.js',
    chequeInfo: './dist/src/client/chequeInfo.js',
    transactionPool: './dist/src/client/transactionPool.js',
    minedBlocks: './dist/src/client/minedBlocks.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle/[name].bundle.js'
  }
};